<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class RoutingTestPhpTest extends WebTestCase
{
    /**
     * @group functional
     * @dataProvider provideRoutes
     */

    public function testARouteIsOk(string $path) :void
    {
        $client = static::createClient();
        $client->request('GET', '/');

        self::assertResponseIsSuccessful();
    }

    public function provideRoutes(): \Generator
    {
        yield['/'];
        yield['/'];
        yield['/'];
    }

}
