<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListeVoituresController extends AbstractController
{
    /**
     * @Route("/liste/voitures", name="listing", methods={"GET"})
     */
    public function index(): Response
    {
        $listing=['peugeot','citroen', 'renault'];

        return $this->render('liste_voitures/index.html.twig', [
            'listing_voiture' => $listing
        ]);
    }

    public function detailVoiture(string $marque): Response
    {
        $detail=["peugeot"=>"detail peugeot","citroen"=>"detail citroen", "renault"=>"detail renault"];

        return $this->render('liste_voitures/index.html.twig', [
            'detail_voiture' => $marque[]
        ]);
    }

}
